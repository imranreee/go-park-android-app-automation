# Why this project?
This is automation project for GoPark android application

# Info
* Project Type: Maven
* Framework: Appium, TestNG, and JUnit
* Design Pattern: Page Object mode with Page factory
* Reporting: Extent Report(http://extentreports.com/)

# Prerequisites to use the project
* Download JDK: http://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html
* Set environment variable for JDK: https://www.mkyong.com/java/how-to-set-java_home-environment-variable-on-mac-os-x/
* Download SDK: https://developer.android.com/studio/index.html
* Set environment variable for SDK:  https://stackoverflow.com/questions/19986214/setting-android-home-enviromental-variable-on-mac-os-x
* Install and run  Appium server: https://appium.io 
* Download IntelliJ IDEA: https://www.jetbrains.com/idea/download/
* Open the given project by IntelliJ IDEA 
