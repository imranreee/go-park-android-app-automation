package settings;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;

import static java.sql.DriverManager.getDriver;

/**
 * Created by imran on 06/12/2018.
 */
public class HelperClass {

    protected WebDriver driver;

    public HelperClass(WebDriver driver) {
        this.driver = driver;
    }


    protected void waitForVisibilityOf(AndroidElement locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(locator));
    }

    public void putAppBackgroundBackForeground(){
        ((AndroidDriver) driver).closeApp();
        System.out.println("Application goes to background");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Application goes to background", ExtentColor.GREEN));
        try{
            ((AndroidDriver)driver).runAppInBackground(Duration.ofMillis(1000));

        }catch (Exception e) {

        }
    }
}
