package settings;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by imran on 06/11/2018.
 */
public class AndroidSetup {
    protected static AndroidDriver driver;

    public static void prepareAndroidForAppium() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "8.1.0");
        capabilities.setCapability("deviceName", "Nexus");
        capabilities.setCapability("autoGrantPermissions", "true");
        //capabilities.setCapability("noReset", "true"); //Enable and disable as per you needs, for onboarding page test it should be disable

        capabilities.setCapability("app", "D:\\GitLab\\GoParkAndroidApp\\testApk\\goPark.apk");
        capabilities.setCapability("appPackage", "com.gopark.debug");
        capabilities.setCapability("appActivity", "com.ford.gopark.router.RouterActivity");
        driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
    }
}
