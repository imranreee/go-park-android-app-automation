package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import settings.HelperClass;
import settings.RunCases;

import java.util.concurrent.TimeUnit;

public class OnBoardingPageSkip extends HelperClass {
    @AndroidFindBy(id = "view_onboarding_text_skip")
    AndroidElement skipBtn;

    @AndroidFindBy(id = "termsAgreeCheckbox")
    AndroidElement termsAgreeCheckbox;

    @AndroidFindBy(id = "termsSubmit")
    AndroidElement termsSubmitBtn;

    @AndroidFindBy(id = "location_button")
    AndroidElement locationBtn;

    By locationBtnNew = By.id("location_button");

    @AndroidFindBy(className = "android.widget.ImageButton")
    AndroidElement navigationDrawer;

    @AndroidFindBy(id = "mapToolbarPrompt")
    AndroidElement hintTextSearchField;

    @AndroidFindBy(id = "menu_map_search")
    AndroidElement searchField;



    public OnBoardingPageSkip(AppiumDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver, 5000, TimeUnit.MILLISECONDS), this);
    }

    public OnBoardingPageSkip onBoardingPageSkip() throws Exception {

        RunCases.test = RunCases.extent.createTest("Onboarding Page skip test");
        waitForVisibilityOf(skipBtn);
        System.out.println("App launched on the Onboarding page");
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("App launched on the Onboarding page", ExtentColor.CYAN));

        skipBtn.click();
        waitForVisibilityOf(termsAgreeCheckbox);
        System.out.println("Skip onboarding worked fine");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Skip onboarding worked fine", ExtentColor.GREEN));

        putAppBackgroundBackForeground();

        waitForVisibilityOf(termsAgreeCheckbox);
        System.out.println("Application back to foreground");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Application back to foreground", ExtentColor.GREEN));

        termsAgreeCheckbox.click();
        termsSubmitBtn.click();
        Thread.sleep(3000);
        Thread.sleep(3000);
        if (driver.findElements(locationBtnNew).size() <= 0){
            System.out.println("Location button not available on the page");
        }else {
            waitForVisibilityOf(locationBtn);
            System.out.println("Agree terms checkbox and Submit button worked and location button found");
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Agree terms checkbox and Submit button worked and location button found", ExtentColor.GREEN));
        }

        waitForVisibilityOf(navigationDrawer);
        System.out.println("Navigation Drawer found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Navigation Drawer found", ExtentColor.GREEN));

        Assert.assertEquals(hintTextSearchField.getText(), "Where are you going?");
        System.out.println("Expected hint text found in search field: "+ hintTextSearchField.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Expected hint text found in search field "+ hintTextSearchField.getText(), ExtentColor.GREEN));

        waitForVisibilityOf(searchField);
        System.out.println("Search Field found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Search Field found", ExtentColor.GREEN));

        new TouchAction((AppiumDriver)driver).press(300,550).release().perform().press(300,550).release().perform();
        new TouchAction((AppiumDriver)driver).press(300,550).release().perform().press(300,550).release().perform();
        new TouchAction((AppiumDriver)driver).press(300,550).release().perform().press(300,550).release().perform();
        System.out.println("Map zoom in");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Map zoom in", ExtentColor.GREEN));

        locationBtn.click();
        Thread.sleep(3000);
        System.out.println("Map zoom out");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Map zoom out", ExtentColor.GREEN));


        return new OnBoardingPageSkip((AppiumDriver) driver);
    }

}
