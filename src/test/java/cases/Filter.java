package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import settings.CaptureScreenShot;
import settings.HelperClass;
import settings.RunCases;

import java.util.concurrent.TimeUnit;

public class Filter extends HelperClass {
    @AndroidFindBy(id = "menu_map_search")
    AndroidElement searchField;

    @AndroidFindBy(id = "locationSearchInput")
    AndroidElement locationSearchInput;

    @AndroidFindBy(id = "locationSearchResultName")
    AndroidElement searchResult;

    @AndroidFindBy(id = "selectedLocationName")
    AndroidElement selectedLocationName;

    @AndroidFindBy(id = "selectedLocationAddress")
    AndroidElement selectedLocationAddress;

    @AndroidFindBy(id = "navigate_search_button")
    AndroidElement navigateBtn;

    @AndroidFindBy(id = "location_button")
    AndroidElement locationBtn;

    @AndroidFindBy(id = "perspective")
    AndroidElement mapRotateBtn;

    @AndroidFindBy(id = "filter_icon")
    AndroidElement filterBtn;

    @AndroidFindBy(id = "price_value")
    AndroidElement priceUpTo;

    @AndroidFindBy(id = "length_value")
    AndroidElement lengthOfSay;

    @AndroidFindBy(id = "length_max")
    AndroidElement maxLength;

    @AndroidFindBy(id = "length_min")
    AndroidElement lengthMin;

    @AndroidFindBy(id = "filter_apply_button")
    AndroidElement applyFilter;

    @AndroidFindBy(id = "filter_bay_count")
    AndroidElement filtersResult;

    @AndroidFindBy(id = "filter_clear_icon")
    AndroidElement filterClearBtn;

    public Filter(AppiumDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver, 5000, TimeUnit.MILLISECONDS), this);
    }

    public Filter filter() throws Exception {

        RunCases.test = RunCases.extent.createTest("Filter page test");

        waitForVisibilityOf(searchField);
        System.out.println("Application on the map page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Application on the map page", ExtentColor.GREEN));

        Thread.sleep(3000);
        searchField.click();
        waitForVisibilityOf(locationSearchInput);
        locationSearchInput.sendKeys("Hammersmith");

        waitForVisibilityOf(searchResult);
        Thread.sleep(3000);
        searchResult.click();
        System.out.println("Search result found and clicked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Search result found and clicked", ExtentColor.GREEN));

        Thread.sleep(3000);
        waitForVisibilityOf(selectedLocationName);
        Assert.assertEquals(selectedLocationName.getText(), "Hammersmith");

        System.out.println("Expected location appeared: "+selectedLocationName.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Expected location appeared: "+selectedLocationName.getText() , ExtentColor.GREEN));


        waitForVisibilityOf(selectedLocationAddress);
        Assert.assertEquals(selectedLocationAddress.getText(), "London");

        System.out.println("Expected location appeared: "+selectedLocationAddress.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Expected location appeared: "+selectedLocationAddress.getText() , ExtentColor.GREEN));

        waitForVisibilityOf(filterBtn);
        filterBtn.click();
        Thread.sleep(2000);

        TouchAction touchAction = new TouchAction((AppiumDriver)driver);
        //Please change according to the device size
        touchAction.tap(170, 440).perform();
        Assert.assertEquals(priceUpTo.getText(), "Up to £1.00 per hour");

        System.out.println("Filter set for Up to £1.00 per hour");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Filter set for Up to £1.00 per hour", ExtentColor.GREEN));

        System.out.println("Price to pick up: " +priceUpTo.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Price to pick up: " +priceUpTo.getText(), ExtentColor.GREEN));

        Assert.assertEquals(lengthOfSay.getText(), "15 min");
        System.out.println("Length stay: " +lengthOfSay.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Length stay: " +lengthOfSay.getText(), ExtentColor.GREEN));

        Assert.assertEquals(lengthMin.getText(), "15 mins");
        System.out.println("Length stay min: " +lengthMin.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Length stay min: " +lengthMin.getText(), ExtentColor.GREEN));

        Assert.assertEquals(maxLength.getText(), "12 hours");
        System.out.println("Max stay: " +maxLength.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Max stay: " +maxLength.getText(), ExtentColor.GREEN));

        applyFilter.click();
        System.out.println("Filter applied");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Filter applied", ExtentColor.GREEN));
        Thread.sleep(3000);

        CaptureScreenShot.capture(driver, "CheckPriceTagAfter1perHour"+RunCases.dateFormat);

        System.out.println("Up to £1.00 per hour filter's result: "+filtersResult);
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Up to £1.00 per hour filter's result: "+filtersResult, ExtentColor.GREEN));

        filterClearBtn.click();
        waitForVisibilityOf(filterBtn);
        System.out.println("Clear filter worked fine");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Clear filter worked fine", ExtentColor.GREEN));
        CaptureScreenShot.capture(driver, "CheckPriceTagAfterResetFilter"+RunCases.dateFormat);

        filterBtn.click();
        waitForVisibilityOf(applyFilter);

        TouchAction touchAction2 = new TouchAction((AppiumDriver)driver);
        //Please change according to the device size
        touchAction2.tap(550, 440).perform();
        Assert.assertEquals(priceUpTo.getText(), "Up to £5.00 per hour");

        System.out.println("Filter set for Up to £5.00 per hour");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Filter set for Up to £5.00 per hour", ExtentColor.GREEN));

        TouchAction touchAction3 = new TouchAction((AppiumDriver)driver);
        touchAction3.tap(258, 720).perform();
        Assert.assertEquals(lengthOfSay.getText(), "4 h ");
        System.out.println("Stay length 4 h");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Stay length 4 h", ExtentColor.GREEN));

        Thread.sleep(1000);
        applyFilter.click();
        Thread.sleep(2000);
        System.out.println("New filter applied");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("New filter applied", ExtentColor.GREEN));

        System.out.println("Up to £5.00 per hour filter's result: "+filtersResult);
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Up to £5.00 per hour filter's result: "+filtersResult, ExtentColor.GREEN));

        CaptureScreenShot.capture(driver, "CheckPriceTagAfter5and4"+RunCases.dateFormat);

        filterClearBtn.click();
        waitForVisibilityOf(filterBtn);
        System.out.println("Clear filter worked fine");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Clear filter worked fine", ExtentColor.GREEN));
        CaptureScreenShot.capture(driver, "CheckPriceTagAfterResetFilter"+RunCases.dateFormat);

        Thread.sleep(2000);
        filterBtn.click();
        Thread.sleep(1000);

        TouchAction touchAction4 = new TouchAction((AppiumDriver)driver);
        touchAction4.tap(75, 440).perform();
        Assert.assertEquals(priceUpTo.getText(), "Free");

        TouchAction touchAction5 = new TouchAction((AppiumDriver)driver);
        touchAction5.tap(650, 720).perform();
        Assert.assertEquals(lengthOfSay.getText(), "12 h");

        System.out.println("Filter set as free and 12 h");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Filter set as free and 12 h", ExtentColor.GREEN));

        applyFilter.click();
        Thread.sleep(3000);

        CaptureScreenShot.capture(driver, "CheckPriceTagAfterFreeAnd12"+RunCases.dateFormat);

        System.out.println("Free and 12 h filter's result: "+filtersResult);
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Free and 12 h filter's result: "+filtersResult, ExtentColor.GREEN));

        filterClearBtn.click();
        waitForVisibilityOf(filterBtn);
        System.out.println("Clear filter worked fine");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Clear filter worked fine", ExtentColor.GREEN));


        return new Filter((AppiumDriver) driver);
    }

}
