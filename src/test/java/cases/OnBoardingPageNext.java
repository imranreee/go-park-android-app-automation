package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import settings.HelperClass;
import settings.RunCases;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static cases.OnBoardingPageSkip.*;

public class OnBoardingPageNext extends HelperClass {
    @AndroidFindBy(id = "view_onboarding_text_next")
    AndroidElement nextBtn;

    @AndroidFindBy(id = "onboarding_get_started_button")
    AndroidElement getStartedBtn;

    @AndroidFindBy(id = "text_message")
    AndroidElement errorMessage;

    @AndroidFindBy(id = "view_onboarding_text_skip")
    AndroidElement skipBtn;

    @AndroidFindBy(id = "termsAgreeCheckbox")
    AndroidElement termsAgreeCheckbox;

    @AndroidFindBy(id = "termsSubmit")
    AndroidElement termsSubmitBtn;

    @AndroidFindBy(id = "location_button")
    AndroidElement locationBtn;

    @AndroidFindBy(className = "android.widget.ImageButton")
    AndroidElement navigationDrawer;

    @AndroidFindBy(id = "mapToolbarPrompt")
    AndroidElement hintTextSearchField;

    @AndroidFindBy(id = "menu_map_search")
    AndroidElement searchField;

    By errorMessage2 = By.id("errorMessage");


    public OnBoardingPageNext(AppiumDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver, 5000, TimeUnit.MILLISECONDS), this);
    }

    public OnBoardingPageNext onBoardingPageNext() throws Exception {

        RunCases.test = RunCases.extent.createTest("Onboarding Page next test");

        ((AndroidDriver)driver).resetApp();

        waitForVisibilityOf(nextBtn);
        System.out.println("App launched on the Onboarding page");
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("App launched on the Onboarding page", ExtentColor.CYAN));

        nextBtn.click();
        waitForVisibilityOf(nextBtn);
        nextBtn.click();
        System.out.println("Skip onboarding worked fine");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Skip onboarding worked fine", ExtentColor.GREEN));

        putAppBackgroundBackForeground();

        waitForVisibilityOf(nextBtn);
        nextBtn.click();
        waitForVisibilityOf(nextBtn);
        nextBtn.click();
        waitForVisibilityOf(getStartedBtn);
        System.out.println("Application back to foreground");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Application back to foreground", ExtentColor.GREEN));

        getStartedBtn.click();
        Thread.sleep(3000);

        waitForVisibilityOf(termsAgreeCheckbox);
        termsAgreeCheckbox.click();
        waitForVisibilityOf(termsSubmitBtn);
        termsSubmitBtn.click();

        waitForVisibilityOf(locationBtn);
        System.out.println("Agree terms checkbox and Submit button worked and location button found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Agree terms checkbox and Submit button worked and location button found", ExtentColor.GREEN));


        waitForVisibilityOf(navigationDrawer);
        System.out.println("Navigation Drawer found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Navigation Drawer found", ExtentColor.GREEN));

        Assert.assertEquals(hintTextSearchField.getText(), "Where are you going?");
        System.out.println("Expected hint text found in search field: "+ hintTextSearchField.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Expected hint text found in search field "+ hintTextSearchField.getText(), ExtentColor.GREEN));

        waitForVisibilityOf(searchField);
        System.out.println("Search Field found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Search Field found", ExtentColor.GREEN));

        Thread.sleep(3000);
        Thread.sleep(3000);
        if (driver.findElements(errorMessage2).size() > 0){
            Assert.assertEquals(errorMessage.getText(), "Sorry, we currently have no data for this area.");

            System.out.println("Error message found: "+ errorMessage.getText());
            RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Error message found: "+ errorMessage.getText(), ExtentColor.GREEN));
        }

        return new OnBoardingPageNext((AppiumDriver) driver);
    }

}
