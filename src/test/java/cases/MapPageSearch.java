package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import settings.HelperClass;
import settings.RunCases;

import java.util.concurrent.TimeUnit;

public class MapPageSearch extends HelperClass {
    @AndroidFindBy(id = "menu_map_search")
    AndroidElement searchField;

    @AndroidFindBy(id = "locationSearchInput")
    AndroidElement locationSearchInput;

    @AndroidFindBy(id = "locationSearchResultName")
    AndroidElement searchResult;

    @AndroidFindBy(id = "selectedLocationName")
    AndroidElement selectedLocationName;

    @AndroidFindBy(id = "selectedLocationAddress")
    AndroidElement selectedLocationAddress;

    @AndroidFindBy(id = "navigate_search_button")
    AndroidElement navigateBtn;

    @AndroidFindBy(id = "location_button")
    AndroidElement locationBtn;

    @AndroidFindBy(id = "perspective")
    AndroidElement mapRotateBtn;

    @AndroidFindBy(id = "filter_icon")
    AndroidElement filterBtn;

    public MapPageSearch(AppiumDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver, 5000, TimeUnit.MILLISECONDS), this);
    }

    public MapPageSearch mapPageSearch() throws Exception {

        RunCases.test = RunCases.extent.createTest("Map page search test");

        waitForVisibilityOf(locationBtn);
        System.out.println("Application on the map page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Application on the map page", ExtentColor.GREEN));

        searchField.click();
        waitForVisibilityOf(locationSearchInput);
        locationSearchInput.sendKeys("Hammersmith");

        waitForVisibilityOf(searchResult);
        Thread.sleep(3000);
        searchResult.click();
        System.out.println("Search result found and clicked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Search result found and clicked", ExtentColor.GREEN));

        Thread.sleep(3000);
        waitForVisibilityOf(selectedLocationName);
        Assert.assertEquals(selectedLocationName.getText(), "Hammersmith");

        System.out.println("Expected location appeared: "+selectedLocationName.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Expected location appeared: "+selectedLocationName.getText() , ExtentColor.GREEN));


        waitForVisibilityOf(selectedLocationAddress);
        Assert.assertEquals(selectedLocationAddress.getText(), "London");

        System.out.println("Expected location appeared: "+selectedLocationAddress.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Expected location appeared: "+selectedLocationAddress.getText() , ExtentColor.GREEN));

        waitForVisibilityOf(navigateBtn);
        System.out.println("Navigate to location button found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Navigate to location button found", ExtentColor.GREEN));

        waitForVisibilityOf(locationBtn);
        System.out.println("Map recenter button found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Map recenter button found", ExtentColor.GREEN));

        waitForVisibilityOf(mapRotateBtn);
        System.out.println("Map rotate button found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Map rotate button found", ExtentColor.GREEN));

        waitForVisibilityOf(filterBtn);
        System.out.println("Filter button found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Filter button found", ExtentColor.GREEN));

        return new MapPageSearch((AppiumDriver) driver);
    }

}
