package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import settings.HelperClass;
import settings.RunCases;

import java.util.concurrent.TimeUnit;

public class MapNavigate extends HelperClass {
    @AndroidFindBy(id = "menu_map_search")
    AndroidElement searchField;

    @AndroidFindBy(id = "locationSearchInput")
    AndroidElement locationSearchInput;

    @AndroidFindBy(id = "locationSearchResultName")
    AndroidElement searchResult;

    @AndroidFindBy(id = "selectedLocationName")
    AndroidElement selectedLocationName;

    @AndroidFindBy(id = "selectedLocationAddress")
    AndroidElement selectedLocationAddress;

    @AndroidFindBy(id = "navigate_search_button")
    AndroidElement navigateBtn;

    @AndroidFindBy(id = "selectedLocationName")
    AndroidElement selectYourLocation;

    @AndroidFindBy(id = "route_preview_destination_textview")
    AndroidElement destination;

    @AndroidFindBy(id = "route_preview_textview_duration")
    AndroidElement timeDuration;

    @AndroidFindBy(id = "route_preview_textview_distance")
    AndroidElement distance;

    @AndroidFindBy(id = "route_preview_back_arrow_imageview")
    AndroidElement backArrow;

    @AndroidFindBy(id = "route_preview_button_start")
    AndroidElement startBtn;

    @AndroidFindBy(id = "text_maneuver_distance")
    AndroidElement text_maneuver_distance;

    @AndroidFindBy(id = "text_maneuver_road")
    AndroidElement text_maneuver_road;

    @AndroidFindBy(id = "image_exit")
    AndroidElement crossBtn;

    @AndroidFindBy(id = "driving_info")
    AndroidElement driving_info;

    @AndroidFindBy(id = "text_destination_distance")
    AndroidElement text_destination_distance;

    @AndroidFindBy(id = "text_eta")
    AndroidElement text_eta;

    @AndroidFindBy(id = "image_maneuver")
    AndroidElement image_maneuver;

    @AndroidFindBy(id = "route_preview_start_imageview")
    AndroidElement recenterBtn;

    @AndroidFindBy(id = "location_button")
    AndroidElement locationBtn;

    @AndroidFindBy(id = "progress")
    AndroidElement loadingspinner;


    public MapNavigate(AppiumDriver driver) {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver, 5000, TimeUnit.MILLISECONDS), this);
    }

    public MapNavigate mapNavigate() throws Exception {

        RunCases.test = RunCases.extent.createTest("Map Navigate test");

        Thread.sleep(3000);
        Thread.sleep(3000);
        waitForVisibilityOf(searchField);
        System.out.println("Application on the map page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Application on the map page", ExtentColor.GREEN));

        searchField.click();
        waitForVisibilityOf(locationSearchInput);
        locationSearchInput.sendKeys("Hammersmith");

        waitForVisibilityOf(searchResult);
        Thread.sleep(3000);
        searchResult.click();
        System.out.println("Search result found and clicked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Search result found and clicked", ExtentColor.GREEN));

        Thread.sleep(3000);
        waitForVisibilityOf(selectedLocationName);
        Assert.assertEquals(selectedLocationName.getText(), "Hammersmith");

        System.out.println("Expected location appeared: "+selectedLocationName.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Expected location appeared: "+selectedLocationName.getText() , ExtentColor.GREEN));


        waitForVisibilityOf(selectedLocationAddress);
        Assert.assertEquals(selectedLocationAddress.getText(), "London");

        System.out.println("Expected location appeared: "+selectedLocationAddress.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Expected location appeared: "+selectedLocationAddress.getText() , ExtentColor.GREEN));

        waitForVisibilityOf(navigateBtn);
        System.out.println("Navigate to location button found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Navigate to location button found", ExtentColor.GREEN));

        navigateBtn.click();
        waitForVisibilityOf(selectYourLocation);

        System.out.println("Your location: "+selectYourLocation.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Your location: "+selectYourLocation.getText(), ExtentColor.GREEN));

        waitForVisibilityOf(destination);
        System.out.println("Your destination: "+destination.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Your destination: "+destination.getText(), ExtentColor.GREEN));

        waitForVisibilityOf(timeDuration);
        System.out.println("Time duration: "+timeDuration.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Time duration: "+timeDuration.getText(), ExtentColor.GREEN));

        waitForVisibilityOf(distance);
        System.out.println("Distance: "+distance.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Distance: "+distance.getText(), ExtentColor.GREEN));

        waitForVisibilityOf(backArrow);
        System.out.println("Back arrow button found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Back arrow button found", ExtentColor.GREEN));

        waitForVisibilityOf(startBtn);
        startBtn.click();
        System.out.println("Start button found and clicked");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Start button found and clicked", ExtentColor.GREEN));

        waitForVisibilityOf(text_maneuver_distance);
        System.out.println("Current distance: "+text_maneuver_distance.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Current distance: "+text_maneuver_distance.getText(), ExtentColor.GREEN));

        waitForVisibilityOf(text_maneuver_road);
        System.out.println("Roda: "+text_maneuver_road.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Roda: "+text_maneuver_road.getText(), ExtentColor.GREEN));

        waitForVisibilityOf(driving_info);
        System.out.println("Driving info: "+driving_info);
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Driving info: "+driving_info, ExtentColor.GREEN));

        waitForVisibilityOf(text_destination_distance);
        System.out.println("Destination distance: "+text_destination_distance.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Destination distance: "+text_destination_distance.getText(), ExtentColor.GREEN));

        waitForVisibilityOf(text_eta);
        System.out.println("ETA: "+text_eta.getText());
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("ETA: "+text_eta.getText(), ExtentColor.GREEN));

        waitForVisibilityOf(image_maneuver);
        System.out.println("Street arrow found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Street arrow found", ExtentColor.GREEN));

        crossBtn.click();
        waitForVisibilityOf(recenterBtn);
        System.out.println("Clicked on the cross button and back to recenter page");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Clicked on the cross button and back to recenter page", ExtentColor.GREEN));

        Thread.sleep(3000);
        backArrow.click();
        Thread.sleep(3000);
        locationBtn.click();

        waitForVisibilityOf(loadingspinner);
        System.out.println("Loading spinner found");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Loading spinner found", ExtentColor.GREEN));


        return new MapNavigate((AppiumDriver) driver);
    }

}
